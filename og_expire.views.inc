<?php

/**
  * @file
  *  Views integration for OG Expiration.
  */

/**
 * Implementation of hook_views_data().
 */
function og_expire_views_data() {
  // Table definition.
  $data['og_expire']['table']['group'] = t('Organic groups');
  $data['og_expire']['table']['join'] = array(
      'node' => array(
        'left_field' => 'nid',
        'field' => 'nid',
        ),
      'users' => array(
        'left_field' => 'uid',
        'field' => 'uid',
        ),
      );
  // Fields.
  $data['og_expire']['expire'] = array(
      'title' => t('OG: Membership expiration date'),
      'help' => t("Membership expiration date."),
      'field' => array(
        'handler' => 'views_handler_field_og_expire',
        'click sortable' => TRUE,
        ),
      'sort' => array(
        'handler' => 'views_handler_sort_date',
        ),
      'filter' => array(
        'handler' => 'views_handler_filter_date',
        ),

      );

  return $data;
}
